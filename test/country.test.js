const axios = require('axios');
const route = 'http://localhost:4000/api/country';
let country = {
    "name": "Chile",
    "flagImageSrc": "https://www.banderas-mundo.es/data/flags/big/cu.png"
};

let newCountry= {
    "name": "Argentina",
    "flagImageSrc": "https://images.emojiterra.com/twitter/512px/1f1e6-1f1f7.png"
};

let invalidCountry = {
    "name": "ir@"
    
};

let modelName = "Countries";
const NON_EXISTING_ID = "5de2b7df02fc5916d0cb898p";

describe(`GET method for ${modelName}`, () => {

    test(`Should GET an array with all ${modelName} items`, async (done) => {
        try {
            const response = await axios.get(route);
            expect(response.status).toBe(200);
            expect(response.data).toEqual(expect.any(Array));
            expect(response.data.length).toBe(0);
            done();
        } catch (error) {
            console.log("Error: ", error);
            done.fail(error);
        }
    })

    test(`Should GET a single ${modelName} item by ID`, async (done) => {
        try {
            await axios.post(route, country);
            let response = await axios.get(route);
            let currentCountry = response.data.find(element => element.name === country.name);
            response = await axios.get(route + '/' + currentCountry._id);
            await axios.delete(route + '/' + currentCountry._id);
            expect(response.status).toBe(200);
            expect(response.data[0].name).toEqual(country.name);
            expect(response.data).toEqual(expect.any(Object));
            done();
        } catch (error) {
            done.fail(error);
        }
    });

    test(`Should NOT GET a ${modelName} item  when Non-existing ID is sent`, async (done) => {
        try {
            const response = await axios.get(route + '/' + NON_EXISTING_ID);
            expect(response.status).toBe(200);
            expect(response.data[0]).toBeUndefined();
            done();
        } catch (error) {
            done.fail(error);
        }
    });

})

describe(`DELETE method for ${modelName}`, () => {

    test(`Should DELETE the ${modelName} item and reply with status 200`, async (done) => {
        try {
            await axios.post(route, country);
            let response = await axios.get(route);
            let currentCountry = response.data.find(element => element.name === country.name);
            await axios.delete(route + '/' + currentCountry._id);
            response = await axios.get(route + '/' + currentCountry._id);
            expect(response.status).toBe(200);
            expect(response.data[0]).toBeUndefined();
            done();
        } catch (error) {
            done.fail(error)
        }
    });

});

describe(`POST method for ${modelName}`, () => {

    test(`Should INSERT a new ${modelName} item`, async (done) => {
        try {
            await axios.post(route, country);
            let response = await axios.get(route);
            expect(response.data.length).toEqual(1);
            expect(response.status).toBe(200);
            let currentCountry = response.data.find(element => element.name === country.name);
            response = await axios.get(route + '/' + currentCountry._id);
            expect(response.data[0].name).toEqual(country.name);
            await axios.delete(route + '/' + currentCountry._id);
            done();
        } catch (error) {
            done.fail(error);
        }
    });

    test(`Should NOT INSERT a new ${modelName} item and reply with status 200 when required fields are empty`, async (done) => {
        try {
            await axios.post(route, invalidCountry);
            let response = await axios.get(route);
            expect(response.data.length).toEqual(0);
            expect(response.status).toBe(200);
            done();
        } catch (error) {
            done.fail(error);
        }
    });

});

describe(`PUT method for ${modelName}`, () => {
    
    test(`Should UPDATE the ${modelName} item by ID with the provided data`, async (done) => {
        try {
            await axios.post(route, country);
            let response = await axios.get(route);
            let currentCountry = response.data.find(element => element.name === country.name);
            newCountry.id = currentCountry._id;
            response = await axios.put(route, newCountry);
            expect(response.status).toBe(200);
            response = await axios.get(route + '/' + currentCountry._id);
            expect(response.data[0].name).toEqual(newCountry.name);
            await axios.delete(route + '/' + currentCountry._id);
            done();
        } catch (error) {
            done.fail(error);
        }
    });

    test(`Should NOT UPDATE the ${modelName} item and reply with status 200 when NON_EXISTING_ID ID is sent`, async (done) => {
        try {
            await axios.post(route, country);
            let response = await axios.get(route);
            let currentCountry = response.data.find(element => element.name === country.name);
            newCountry.id = NON_EXISTING_ID;
            response = await axios.put(route, newCountry);
            expect(response.status).toBe(200);
            response = await axios.get(route + '/' + currentCountry._id);
            expect(response.data[0].name).toEqual(country.name);
            await axios.delete(route + '/' + currentCountry._id);
            done();
        } catch (error) {
            done.fail(error);
        }
    });

});

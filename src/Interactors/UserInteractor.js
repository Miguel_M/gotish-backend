const UserValidator = require('../Entities/Validators/UserValidator');


class UserInteractor {

    constructor(userRepository) {
        this.userRepository = userRepository;
    }

    async getOne(id) {
        let user = await this.userRepository.getUserAccountById(id);
        return user;
    }

    async getAll() {
        return await this.userRepository.getAll();
        // return await this.userRepository.getAllUserAccounts();
    }

    async create(user) {
        let userValidator = new UserValidator();
        let response = userValidator.validateCreate(user);
        if(!response.success)
            return response;
        try {
            // this.userRepository.saveUserAccount(user);
            this.userRepository.insert(user);
            
        } catch (error) {
            return this.prepareMongoErrorResponse(error);
        }
        return response
    }

    async delete(idAuth) {
        let response;
        try {
            // this.userRepository.deleteUserAccount(idAuth);
           this.userRepository.delete(idAccountData);
            response = this.prepareMongoDeleteResponse(idAuth, response);

        } catch (error) {
            return this.prepareMongoErrorResponse(error);
        }
        return response;
    }

    async update(idAuth, user) {
        let userValidator = new UserValidator();
        let response = userValidator.validateUpdate(idAuth, user);
        if (!response.success) {
            return response;
        }
        try {
            // this.userRepository.updateUserAccount(idAuth, user);
           this.userRepository.update(idAccountData, user);
        } catch (error) {
            return this.prepareMongoDeleteResponse(error);
        }
        return response;
    }
    
    prepareMongoDeleteResponse(id, response) {
        response = { success: true, message: "" };
        response.message = "The item " + id + " was deleted successfully" 
        return response
    }
    
    prepareMongoErrorResponse(error) {
        return { success: false, message: error.toString() };
    }

}

module.exports = UserInteractor;
const admin = require("firebase-admin");

class UserRepository {

    constructor(DBConection) {
            this.DBConection = DBConection;
            this.collection = admin.firestore().collection('user');
    }

    async getOne(searchCriteria) {
        return await this.DBConection.getOne(searchCriteria, this.collection)
            .catch(error => {
                console.log("Error in getOne on UserRepository: ", error);
            });
    }
    
    async getAll() {
        return await this.DBConection.getAll(this.collection)
            .catch(error => {
                console.log("Error in getAll on UserRepository: ", error);
            });
    }
    
    async insert(object) {
        return await this.DBConection.insert(object, this.collection)
            .catch(error => {
                console.log("Error in insert on UserRepository: ", error);
            });
    }

    async saveUserAccount(object) {
        return await admin.auth().createUser({
            email: object.email,
            password: '123456',
            displayName: object.username
          })
            .then(function(userRecord) {
              console.log('Successfully created new user:', userRecord.uid);
              return true;
            })
            .catch(function(error) {
              console.log('Error creating new user:', error);
            });
    }

    async updateUserAccount(idAuth, user){
        admin.auth().updateUser(idAuth, {
            email: user.$set.email,
            password: user.$set.password,
            displayName: user.$set.username
          })
            .then(function(userRecord) {
              console.log('Successfully updated user');
            })
            .catch(function(error) {
              console.log('Error updating user:', error);
            });
    }

    async deleteUserAccount(uid) {
      return await admin.auth().deleteUser(uid)
        .then(function() {
          console.log('Successfully deleted user');
        })
        .catch(function(error) {
          console.log('Error deleting user:', error);
        });
    }
    async getAllUserAccounts(nextPageToken) {
      let users = Array(0);
     return admin.auth().listUsers(1000, nextPageToken)
        .then(function(listUsersResult) {
          listUsersResult.users.forEach(function(userRecord) {
            users.push({
              'id': userRecord.toJSON().uid,
              'email': userRecord.toJSON().email,
              'username': userRecord.toJSON().displayName
            });
          })
          if (listUsersResult.pageToken) {
            listAllUsers(listUsersResult.pageToken);
          }
          console.log(users)
          return users;
        })
        .catch(function(error) {
          console.log('Error listing users:', error);
        });
    }
    
    async getUserAccountById(id){
      return admin.auth().getUser(id)
        .then(function(userRecord) {
          return  {
            'id': userRecord.toJSON().uid,
            'email': userRecord.toJSON().email,
            'username': userRecord.toJSON().displayName
          }
        })
        .catch(function(error) {
          console.log('Error fetching user data:', error);
        });
    }

    async update(searchCriteria, object) {
        return await this.DBConection.update(searchCriteria, object, this.collection)
            .catch(error => {
                console.log("Error in update on UserRepository: ", error);
            });
    }
    
    async delete(searchCriteria) {
        return await this.DBConection.delete(searchCriteria, this.collection)
            .catch(error => {
                console.log("Error in delete on UserRepository: ", error);
            });
    }
}

module.exports = UserRepository;
const express = require('express');
const app = express();
const bodyParser = require('body-parser')
const cors = require('cors')
const http = require('http');

const DBConectionFactory = require('../Entities/Factories/DBConectionFactory');


app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());

var serverPort = process.env.PORT || '4000';
app.set('port', serverPort);


var DBConection;
(async function () {
  

    FireBaseConection = await DBConectionFactory.createConection('firebase')
        .catch(error => {
            console.log("Error creating connection: ", error);
        });
    let CONECTIONS = {
        database: DBConection,
        firebase: FireBaseConection,
    }
    module.exports = CONECTIONS;
    const routes = require('./routes');
    app.use('/api', routes);
    var server = http.createServer(app);
    server.listen(serverPort, function () {
        console.log('Running on port ' + serverPort);
    });
})();

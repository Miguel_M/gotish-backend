const CreateUserRequestModel = require('./CreateUserRequestModel');

class UpdateUserRequestModel {

    getRequestModel(body) {
        let UserRequestModelInstance = new CreateUserRequestModel();
        let userRequestModel = UserRequestModelInstance.getRequestModel(body);
        return { $set: userRequestModel }
    }
}

module.exports = UpdateUserRequestModel;
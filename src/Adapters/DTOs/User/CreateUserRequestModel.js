class CreateUserRequestModel {

    getRequestModel(body) {
        let name = body.name ? body.name.trim() : "";
        let lastname = body.lastname ? body.lastname.trim() : "";
        let email = body.email ? body.email.trim() : "";
        let career = body.career ? body.career.trim() : "";
        
        let parsedUser = {
            lastname: lastname,
            name: name, email: email,
            career:career
        };
        return parsedUser;
    } 
}

module.exports = CreateUserRequestModel;
class WordValidator {

    validateCreate(user) {
        return this.validate(user);
    }

    validateUpdate(objectID, user) {
        let response = { success: true, message: "" };
        let mongoIdresponse = this.validateMongoID(objectID);
        let responseValidator = this.validate(user.$set);
        if(!(mongoIdresponse )){
            response = { success: false,  message: "Error validating MongoID"};
        }
        if(!responseValidator.success){
            response = responseValidator;
        }
        return response;
    }

    validate(user) {
        let response = { success: true, message: "" };
        if(!this.validateName(user.lastname)){
            response = { success: false,  message: "Error validating lastname"};
        }
        if(!this.validateName(user.name)){
            response = { success: false,  message: "Error validating name"};
        }
        if(!this.validateName(user.career)){
            response = { success: false,  message: "Error validating name"};
        }
        if(!this.validateEmail(user.email)){
            response = { success: false,  message: "Error validating email"};
        }
        return response
    }

    validateMongoID(objectID) {
        return objectID;
    }

    validateName(name) {
        return name && name.length != 0 && !this.nonStandarsAsciiArePresent(name)
            && (name.length < 200);
    }

    validateUsername(username) {
        return username && username.length != 0 && !this.nonStandarsAsciiArePresent(username)
            && (username.length < 200);
    }

    validateUserID(userID) {
        return userID && userID.length != 0 && !this.nonStandarsAsciiArePresent(userID)
            && (userID.length < 500);
    }

    validateEmail(email) {
        var regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return regex.test(email.toLowerCase());

    }

    nonStandarsAsciiArePresent(word) {
        let regex = /[^\x00-\x7F]+/;
        return regex.test(word);
    }
}

module.exports = WordValidator;
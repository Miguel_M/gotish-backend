const MongoDBConection = require('../../Adapters/DBConection/MongoDBConection');
const FirebaseConection = require('../../Adapters/DBConection/FirebaseConection');

class DBConectionFactory {
    constructor() { }

    static async createConection(databaseTecnology, url, databaseName) {
        switch (databaseTecnology) {
            case "mongo":
                const mongo = new MongoDBConection();
                mongo.client = await mongo.connect(url)
                    .catch(error => {
                        console.log("ERROR in connect on DBConectionFactory: ", error);
                    });
                mongo.dataBase = mongo.client.db(databaseName);
                return mongo;
            case "firebase":
                const firebase = new FirebaseConection();
                firebase.client =  firebase.connect();
                return firebase;
            default:
                return null;
        }
    }
}

module.exports = DBConectionFactory;
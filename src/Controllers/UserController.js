const express = require('express');
const router = express.Router();
const UserRepository = require('../Adapters/Repositories/UserRepository');

let CONECTIONS = require('../Adapters/server.js');

var DBConection;
var userRepository;

const CreateUserRequestModel = require("../Adapters/DTOs/User/CreateUserRequestModel");
const UpdateUserRequestModel = require('../Adapters/DTOs/User/UpdateUserRequestModel')
const UserInteractor = require('../Interactors/UserInteractor');

(async function () {
    userRepository = new UserRepository(CONECTIONS.firebase);
})()

router.get("", async function (request, response) {
    let userInteractor = new UserInteractor(userRepository);
    let users = await userInteractor.getAll()
        .catch(error => {
            console.log("ERROR getting all users: ", error);
        });
    response.send(users);
});

router.get("/:id", async function (request, response) {
    let id = request.params.id;
    let userInteractor = new UserInteractor(userRepository);
    let user = await userInteractor.getOne(id)
        .catch(error => {
            console.log("ERROR getting a user: ", error);
        });
    response.send(user);
});

router.post("", async function (request, response) {
    let userRequestModel = new CreateUserRequestModel();
    let insertUserInteractor = new UserInteractor(userRepository);
    let requestModel = userRequestModel.getRequestModel(request.body)
    let isInsertedUser = false;
    try {
        isInsertedUser = await insertUserInteractor.create(requestModel);
    } catch (error) {
        console.log("ERROR inserting a user", error);
    }
    response.send(isInsertedUser);
});

router.put("/:id", async function (request, response) {
    let id = request.params.id;
    let updateUserRequestModel = new UpdateUserRequestModel();
    let requestModel = updateUserRequestModel.getRequestModel(request.body);
    let userInteractor = new UserInteractor(userRepository);
    let putResponse = false;
    try {
        putResponse = await userInteractor.update(id, requestModel);
    } catch (error) {
        console.log("ERROR updating a user", error);
    }
    response.send(putResponse);
});

router.delete("/:id", async function (request, response) {
    let id = request.params.id;
    let userInteractor = new UserInteractor(userRepository);
    let deleteResponse = false;
    try {
        deleteResponse = await userInteractor.delete(id);
    } catch (error) {
        console.log("ERROR deleting a user");
    }
    response.send(deleteResponse);
});

module.exports = router;